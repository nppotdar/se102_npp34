// ****************************************************************
// Dog.java
//
// A class that holds a dog's name and can make it speak.
//          
// ****************************************************************
public class Dog
{
    protected String name;
    protected int breedWeight;
     
    // Constructor -- store name 
    public Dog(String name)
    {
	this.name = name;
	this.breedWeight = 10;
    }

    public Dog(String name, int breedWeight){
	this.name = name;
	this.breedWeight = breedWeight;
    }
       
    // Returns the dog's name
    public String getName()
    {
	return name;
    }

       
    // Returns a string with the dog's comments  
    public String speak()
    {
	return "Woof";
    }

    // Returns weight
    public int avgBreedWeight()
    {
	return breedWeight;
    }

}
