// ****************************************************************
// DogTest.java
//
// A simple test class that creates a Dog and makes it speak.
//          
// ****************************************************************

public class DogTest
{
    public static void main(String[] args)
    {
	Dog dog1 = new Dog("Spike");
	System.out.println(dog1.getName() + " says " + dog1.speak());
	
	Labrador dog2 = new Labrador("Neena", "grey");
	System.out.println(dog2.getName() + " says " + dog2.speak());
	System.out.println(dog2.avgBreedWeight());
	System.out.println(dog2.getColor());

	Yorkshire dog3 = new Yorkshire("Tina");
	System.out.println(dog3.getName() + " says " + dog3.speak());
	System.out.println(dog3.avgBreedWeight());
	



    }
}
