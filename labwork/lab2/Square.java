import java.util.Scanner;
public class Square{

    int[][] square;
    int size;

    //constructor; constructs the 2d array
    public Square(int size)
    {
	this.size = size;
	square = new int[this.size][this.size];
    }

    //sums up a specific row
    public int sumRow(int rowIndex){
	int aggr = 0;
	for( int i = 0; i < this.size; i++ )
	    aggr += this.square[rowIndex][i];
	return aggr;
   }

    //sums up a specific column
    public int sumColumn(int columnIndex){
	int aggr = 0;
	for( int i = 0; i < this.size; i++ )
	    aggr += this.square[i][columnIndex];
	return aggr;	
    }

    public int sumMainDiag(){
	int aggr = 0;
	for( int i = 0; i < this.size; i++ )
	    aggr += this.square[i][i];
	return aggr;
    }

    public int sumOtherDiag(){
	int aggr = 0;
	for( int i = 0 ; i < this.size; i++ )
	    aggr += this.square[i][this.size-1-i];
	return aggr;
    }

    //returns boolean: true if magic square
    //                 false if not magic square
    public boolean magic(){
	int sum = this.sumRow(0);
	for( int i = 0; i < this.size; i++ ){
	    if( sum != this.sumRow(i) )
		return false;
	    if( sum != this.sumColumn(i) )
		return false;
	 }

	if( this.sumMainDiag() != sum )
	    return false;
	
	if( this.sumOtherDiag() != sum )
	    return false;

	return true;
    }

    public void readSquare(Scanner in){

	for(int i = 0; i < this.size; i++){
	    for(int j = 0; j < this.size;j++){
		// note that Scanner inputs does not depend on the format of the input row
		// it ignores whitespace or newline
		this.square[i][j] = in.nextInt();
	    }
	}  

    }
    
    public void printSquare(){
	for(int i = 0; i < this.size; i++){
	    for(int j = 0; j < this.size;j++){
		//print every column for the row i
		System.out.print(this.square[i][j] + " ");
	    }
	    //a line break after each row
	    System.out.println();
	}
    }
    
}
