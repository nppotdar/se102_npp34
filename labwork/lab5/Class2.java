public class Class2 extends Class1
{
    protected InnerClass2 ic;
    public Class2()
    {
	ic = new InnerClass2();
    }
    static public void main(String[] args)
    {
	Class2 c1 = new Class2();
	c1.displayStrings();
    }
    @Override
    public void displayStrings()
    {
	System.out.println(ic.getString() + ".");
	System.out.println(ic.getAnotherString() + ".");
    }
    protected class InnerClass2 extends Class1.InnerClass1
    {
	public String getString()
	{
	    return "InnerClass2: getString invoked";
	}
	public String getAnotherString()
	{
	    return "InnerClass2: getAnotherString invoked";
	}
    }
}
