import java.io.Serializable;
class Dragonite extends Pokemon 
    implements Serializable{
    
    public Dragonite(){
	super();
    }

    public Dragonite(int ownerId, String name, 
		    String nature, char sex){
	super(ownerId, name, 
	      nature, sex);
    }

}
