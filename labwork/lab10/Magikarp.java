import java.io.Serializable;
class Magikarp extends Pokemon 
    implements Serializable{
    
    public Magikarp(){
	super();
    }

    public Magikarp(int ownerId, String name, 
		    String nature, char sex){
	super(ownerId, name, 
	      nature, sex);
    }
}
