import java.io.*;
import java.util.*;

class PokemonCenter implements Serializable{
    ArrayList<Pokemon> pokemons = new ArrayList<Pokemon>();
    static String backupFile = "backupFile.bin";

    public static void main(String args[]){
	// We will be working on:
	// Input Pokemon Details from the user
	PokemonCenter pokemonCenter;

	// Part 1:
	// At the start of the program, we first check whether a backup file already exists.
	//     If it does, then we load it.

	try{
	    pokemonCenter = readObject();
	    System.out.println("**Found earlier records!**");
	    System.out.println("*\nRecords: " + Arrays.toString(pokemonCenter.pokemons.toArray()));
	}catch(Exception e){
	    pokemonCenter = new PokemonCenter();
	}
	
	Scanner in = new Scanner(System.in);
	
	
	while(true){
	    System.out.println("*****\nPlease enter Pokemon details:");
	    System.out.print("Owner ID: ");
	    int ownerId = Integer.parseInt(in.nextLine());

	    System.out.print("Standard Name: ");
	    String standardName = in.nextLine();

	    System.out.print("Nick Name: ");
	    String nickName = in.nextLine();
	    System.out.print("Nature: ");
	    String nature = in.nextLine();
	    System.out.print("Sex: ");
	    char sex = in.next().charAt(0);
	    
	    try{
		Pokemon pokemonToAdd = returnPokemon(standardName, ownerId, nickName, nature, sex);
		pokemonCenter.pokemons.add(pokemonToAdd);
	    }catch(Exception e){
		System.out.println("Sorry. We do not have the required facility to admit this type of Pokemon.");
	    }
	    pokemonCenter.writeObject();
	    System.out.println("*\nRecords: " + Arrays.toString(pokemonCenter.pokemons.toArray()));
	    in.nextLine();
	}
    }

    public static Pokemon returnPokemon(String standardName, int ownerId, String nickName, String nature, char sex) throws Exception{
	if( standardName.equals((new Dragonite()).getClass().getSimpleName()) )
	    return new Dragonite(ownerId, nickName, nature, sex);
	
	if( standardName.equals((new Magikarp()).getClass().getSimpleName()) )
	    return new Magikarp(ownerId, nickName, nature, sex);
	
	throw new Exception();
    }
    


    // For more details on the following functions,
    //     got to: http://goo.gl/gJc6MN
    public void writeObject(){
	// save the object to file
	FileOutputStream fos = null;
	ObjectOutputStream out = null;
	try {
	    fos = new FileOutputStream(backupFile);
	    out = new ObjectOutputStream(fos);
	    out.writeObject(this);

	    out.close();
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }

    public static PokemonCenter readObject() throws Exception{
	// read the object from file
	// save the object to file
	FileInputStream fis = null;
	ObjectInputStream in = null;
	PokemonCenter p = null;
	fis = new FileInputStream(backupFile);
	in = new ObjectInputStream(fis);
	p = (PokemonCenter) in.readObject();
	in.close();
	return p;
    }

    
}
