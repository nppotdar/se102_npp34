import java.io.Serializable;
class Pokemon implements Serializable{

    int ownerId;

    String name;
    String nature;
    char sex;

    public Pokemon(){
    }
    public Pokemon(int ownerId, String name, 
                       String nature, char sex){
	this.name = name;
	this.ownerId = ownerId;
	this.sex = sex;
	this.nature = nature;
    }

    @Override
    public String toString(){
	return this.getClass().getSimpleName() 
	    + "(" + this.name + ")" 
	    + "@" + this.ownerId;
    }

}
